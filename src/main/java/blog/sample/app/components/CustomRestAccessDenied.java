package blog.sample.app.components;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

@Component
public class CustomRestAccessDenied implements AccessDeniedHandler {
    private static final Logger logger = LoggerFactory.getLogger(CustomRestAccessDenied.class);

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        logger.error("Responding with Access Denied error. Message - {}", accessDeniedException.getMessage());
        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        try {
            response.getWriter().write(new JSONObject()
                    .put("timestamp", LocalDateTime.now())
                    .put("description", "Access denied")
                    .put("code", HttpServletResponse.SC_FORBIDDEN)
                    .toString());
        } catch (JSONException e) {
            logger.error("Error generating forbidden response message - {}", e.getMessage());
        }
    }
}
