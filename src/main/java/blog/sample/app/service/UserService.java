package blog.sample.app.service;

import blog.sample.app.dto.UserDto;
import blog.sample.app.model.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.UUID;

public interface UserService {
    /**
     * Register new user
     * @param userDto
     * @return
     */
    User createUser(UserDto userDto) throws IOException;

    User updateUser(UserDto userDto, UUID userId);

    /**
     * Return all existing users
     * @return
     */
    List<User> findAllUsers();

    /**
     * Find user by id
     * @param id
     * @return
     */
    User findUserById(UUID id);

    /**
     * Find an existing user by email
     * @param email
     * @return
     */
    User findUserByEmail(String email);

    /**
     * Delete user
     * @param id
     * @return
     */
    void deleteUser(UUID id);

    boolean isUserInCommunity(String userEmail, String communityName);

//    User uploadProfilePicture(String userEmail, MultipartFile image) throws IOException;
}
