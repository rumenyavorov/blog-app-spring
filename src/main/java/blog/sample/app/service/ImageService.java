package blog.sample.app.service;

import blog.sample.app.dto.ImageDto;
import blog.sample.app.model.Image;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

public interface ImageService {
    Image userProfilePictureUpload(MultipartFile imageFile, String userEmail) throws IOException;

    Image postPictureUpload(MultipartFile imageFile, Long postId) throws IOException;

    Image communityPictureUpload(MultipartFile imageFile, String communityName) throws IOException;
}
