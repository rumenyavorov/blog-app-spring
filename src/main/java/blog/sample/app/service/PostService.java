package blog.sample.app.service;

import blog.sample.app.dto.PostDto;
import blog.sample.app.model.Post;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public interface PostService {
    /**
     * Creates new post
     * @param postDto
     * @return
     */
    Post createPost(PostDto postDto) throws IOException;

    /**
     * Returns all existing posts
     * @return
     */
    List<Post> getAllPosts();

    /**
     * Returns all posts created by *Author*
     * @param author
     * @return
     */
    List<Post> getAllPostsByAuthor(UUID author);

    /**
     * Delete post by id
     * @param id
     * @return
     */
    void deletePost(Long id);

    /**
     * Update post
     * @param id
     * @param postDto
     * @return
     */
    Post updatePost(Long id, PostDto postDto);

    /**
     * Return all posts from the communities user has joined
     * @param userId
     * @return
     */
    List<Post> getAllPostsByJoinedCommunities(UUID userId);

    Post findPostById(Long postId);
}
