package blog.sample.app.service;

import blog.sample.app.dto.CommunityDto;
import blog.sample.app.model.Community;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public interface CommunityService {
    /**
     * Create new community
     * @param communityDto
     * @return
     */
    Community createCommunity(CommunityDto communityDto) throws IOException;

    /**
     * Return all existing communities
     * @return
     */
    List<Community> getAllCommunities();

    /**
     * Checks if community is already created with that name
     * @param email
     * @return
     */
    Community findCommunityByName(String email);

    /**
     * Delete community by id:
     * @param communityId
     */
    void deleteCommunity(Long communityId);

    /**
     *  Join community
     * @param communityId
     * @param userId
     * @return
     */
    Community joinCommunity(Long communityId, UUID userId);

    /**
     * Leave community
     * @param id
     * @return
     */
    Community leaveCommunity(Long id);

    /**
     * Returns specific user joined communities
     * @param userId
     * @return
     */
    List<Community> getJoinedCommunities(UUID userId);

    /**
     * Load community details by communityName
     * @param communityName
     * @return
     */
    Community viewCommunity(String communityName);
}
