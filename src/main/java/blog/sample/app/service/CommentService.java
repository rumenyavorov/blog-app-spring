package blog.sample.app.service;

import blog.sample.app.dto.CommentDto;
import blog.sample.app.model.Comment;

import java.util.List;
import java.util.UUID;

public interface CommentService {
    Comment createComment(Long postId, CommentDto commentDto);

    void deleteComment(Long commentId);

    Comment updateComment(Long commentId, CommentDto commentDto);

    Comment findCommentById(Long commentId);

    List<Comment> findAllCommentsForPost(Long postId);

    List<Comment> findAllCommentsByUser(UUID userId);
}
