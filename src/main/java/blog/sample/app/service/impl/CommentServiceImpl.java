package blog.sample.app.service.impl;

import blog.sample.app.dto.CommentDto;
import blog.sample.app.exception.ApiRequestException;
import blog.sample.app.model.Comment;
import blog.sample.app.model.Post;
import blog.sample.app.model.User;
import blog.sample.app.repository.CommentRepository;
import blog.sample.app.service.CommentService;
import blog.sample.app.service.PostService;
import blog.sample.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class CommentServiceImpl implements CommentService {
    private final UserService userService;

    private final PostService postService;

    private final CommentRepository commentRepository;

    @Autowired
    public CommentServiceImpl(UserService userService, PostService postService, CommentRepository commentRepository) {
        this.userService = userService;
        this.postService = postService;
        this.commentRepository = commentRepository;
    }

    @Override
    public Comment createComment(Long postId, CommentDto commentDto) {
        Comment comment = new Comment();

        User loggedInUser = (User) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User user = userService.findUserByEmail(loggedInUser.getEmail());

        Post post = postService.findPostById(postId);

        comment.setCommentBody(commentDto.getCommentBody());
        comment.setUser(user);
        comment.setPost(post);
        comment.setDate(LocalDateTime.now());

        commentRepository.save(comment);

        return comment;
    }

//    @Override
//    public void deleteComment(Long commentId, Long postId) {
//        List<Comment> commentsForPost = commentRepository.findAllCommentsForPost()
//    }

    @Override
    public void deleteComment(Long commentId) {
        Comment comment = commentRepository.findCommentById(commentId);
        if(comment == null) {
            throw new ApiRequestException("Comment doesn't exist!");
        } else {
            commentRepository.deleteById(commentId);
        }
    }

    @Override
    public Comment updateComment(Long commentId, CommentDto commentDto) {
        return null;
    }

    @Override
    public Comment findCommentById(Long commentId) {
        return null;
    }

    @Override
    public List<Comment> findAllCommentsForPost(Long postId) {
        return commentRepository.findAllCommentsForPost(postId);
    }

    @Override
    public List<Comment> findAllCommentsByUser(UUID userId) {
        List<Comment> comments = commentRepository.findAllCommentsByUser(userId);
        return comments;
    }
}
