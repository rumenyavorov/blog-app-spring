package blog.sample.app.service.impl;

import blog.sample.app.model.Community;
import blog.sample.app.model.Image;
import blog.sample.app.model.Post;
import blog.sample.app.model.User;
import blog.sample.app.repository.ImageRepository;
import blog.sample.app.service.CommunityService;
import blog.sample.app.service.ImageService;
import blog.sample.app.service.PostService;
import blog.sample.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class ImageServiceImpl implements ImageService {
    private final UserService userService;
    private final ImageRepository imageRepository;
    private final PostService postService;
    private final CommunityService communityService;

    @Autowired
    public ImageServiceImpl(UserService userService, ImageRepository imageRepository, PostService postService, CommunityService communityService) {
        this.userService = userService;
        this.imageRepository = imageRepository;
        this.postService = postService;
        this.communityService = communityService;
    }

    @Override
    public Image userProfilePictureUpload(MultipartFile imageFile, String userEmail) throws IOException {
        Image image = new Image();
        User user = userService.findUserByEmail(userEmail);

        image.setImageName(imageFile.getOriginalFilename());
        image.setImageType(imageFile.getContentType());
        image.setImage(imageFile.getBytes());
        user.setUserProfilePicture(image);

        imageRepository.save(image);

        return image;
    }

    @Override
    public Image postPictureUpload(MultipartFile imageFile, Long postId) throws IOException {
        Post post = postService.findPostById(postId);
        Image image = new Image();

        image.setImageName(imageFile.getOriginalFilename());
        image.setImageType(imageFile.getContentType());
        image.setImage(imageFile.getBytes());

        post.setPostImage(image);

        imageRepository.save(image);

        return image;
    }

    @Override
    public Image communityPictureUpload(MultipartFile imageFile, String communityName) throws IOException {
        Community community = communityService.findCommunityByName(communityName);
        Image image = new Image();

        image.setImageName(imageFile.getOriginalFilename());
        image.setImageType(imageFile.getContentType());
        image.setImage(imageFile.getBytes());


        return null;
    }
}
