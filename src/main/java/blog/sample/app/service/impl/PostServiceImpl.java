package blog.sample.app.service.impl;

import blog.sample.app.dto.PostDto;
import blog.sample.app.exception.ApiRequestException;
import blog.sample.app.model.*;
import blog.sample.app.repository.PostRepository;
import blog.sample.app.repository.UserRepository;
import blog.sample.app.service.CommentService;
import blog.sample.app.service.CommunityService;
import blog.sample.app.service.PostService;
import blog.sample.app.service.UserService;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private UserService userService;

    @Autowired
    private CommunityService communityService;

    @Autowired
    private PostRepository postRepository;

    @Override
    public Post createPost(PostDto postDto) throws IOException {
        Post post = new Post();

        User loggedInUser = (User) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();

        User userAuthor = this.userService.findUserByEmail(loggedInUser.getEmail());
        Community community = this.communityService.findCommunityByName(postDto.getCommunity().getName());

        byte[] imageContent = Files.readAllBytes(Paths.get("src/web/images/default-post.png"));
        Image postDefaultAvatar = new Image(imageContent, "default-post.png", "image/png");

        post.setTitle(postDto.getTitle());
        post.setBody(postDto.getBody());
        post.setAuthor(userAuthor);
        post.setCommunity(community);
        post.setDateAdded(LocalDateTime.now());
        post.setPostImage(postDefaultAvatar);

        postRepository.save(post);

        return post;
    }

    @Override
    public List<Post> getAllPosts() {
        return postRepository.getAllPosts();
    }

    @Override
    public List<Post> getAllPostsByAuthor(UUID author) {
        return postRepository.getAllPostsByAuthor(author);
    }

    @Override
    public void deletePost(Long id) {
        postRepository.deleteById(id);
    }

    @Override
    public Post updatePost(Long id, PostDto postDto) {
        Post post = postRepository.findById(id).orElse(null);
        if(post != null) {
            post.setTitle(postDto.getTitle());
            post.setBody(postDto.getBody());
            post.setCommunity(postDto.getCommunity());
            postRepository.save(post);
        } else {
            throw new ApiRequestException("Post not found.");
        }

        return post;
    }

    @Override
    public List<Post> getAllPostsByJoinedCommunities(UUID userId) {
        List<Post> posts = postRepository.getAllPostsByUserJoinedCommunities(userId);
        return posts;
    }

    @Override
    public Post findPostById(Long postId) {
        Post post = postRepository.findPostById(postId);
        if(post == null) {
            throw new ApiRequestException("Post not found");
        }
        return post;
    }
}
