package blog.sample.app.service.impl;

import blog.sample.app.dto.CommunityDto;
import blog.sample.app.exception.ApiRequestException;
import blog.sample.app.model.Community;
import blog.sample.app.model.Image;
import blog.sample.app.model.User;
import blog.sample.app.repository.CommunityRepository;
import blog.sample.app.repository.UserRepository;
import blog.sample.app.service.CommunityService;
import blog.sample.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class CommunityServiceImpl implements CommunityService {

    @Autowired
    private CommunityRepository communityRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public Community createCommunity(CommunityDto communityDto) throws IOException {

        User loggedInUser = (User) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();

        Community community = new Community();
        User communityAuthor = userService.findUserByEmail(loggedInUser.getEmail());
        byte[] imageContent = Files.readAllBytes(Paths.get("src/web/images/default-community.png"));
        Image communityDefaultAvatar = new Image(imageContent, "default-community.png", "image/png");

        if(communityRepository.findCommunityByName(communityDto.getName()) != null){
            throw new ApiRequestException("Community already exist!");
        }
        community.setName(communityDto.getName());
        community.setDescription(communityDto.getDescription());
        community.setDateAdded(LocalDateTime.now());
        community.setCommunityAuthor(communityAuthor);
        community.setCommunityImage(communityDefaultAvatar);

        communityRepository.save(community);

        return community;
    }

    @Override
    public List<Community> getAllCommunities() {
        return communityRepository.findAllCommunities();
    }

    @Override
    public Community findCommunityByName(String email) {
        return communityRepository.findCommunityByName(email);
    }

    @Override
    public void deleteCommunity(Long communityId) {
        communityRepository.deleteById(communityId);
    }

    @Override
    public Community joinCommunity(Long communityId, UUID userId) {

        User currentUser = userRepository.findUserById(userId);
        Community community = communityRepository.findCommunityById(communityId);
        List<Community> communities = new ArrayList<>();
        communities.add(community);
        if(currentUser.getCommunities().contains(community)) {
            throw new ApiRequestException("Community already joined");
        } else {
            currentUser.getCommunities().add(communities.get(0));
            userRepository.save(currentUser);

            return community;
        }
    }

    @Override
    public Community leaveCommunity(Long id) {
        User loggedInUser = (User) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User user = userService.findUserByEmail(loggedInUser.getEmail());
        Community community = communityRepository.findCommunityById(id);
        user.removeFromCommunity(community);
        userRepository.save(user);
        return community;
    }


    @Override
    public List<Community> getJoinedCommunities(UUID userId) {
        List<Community> communities = communityRepository.getJoinedCommunities(userId);
        return communities;
    }

    @Override
    public Community viewCommunity(String communityName) {
        Community community = communityRepository.findCommunityByName(communityName);
        if(community == null) {
            throw new ApiRequestException("Community doesn't exist");
        }
        return community;
    }

}
