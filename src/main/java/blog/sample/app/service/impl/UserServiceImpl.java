package blog.sample.app.service.impl;

import blog.sample.app.dto.UserDto;
import blog.sample.app.exception.ApiRequestException;
import blog.sample.app.model.Image;
import blog.sample.app.model.User;
import blog.sample.app.repository.CommunityRepository;
import blog.sample.app.repository.RoleRepository;
import blog.sample.app.repository.UserRepository;
import blog.sample.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final RoleRepository roleRepository;

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(BCryptPasswordEncoder bCryptPasswordEncoder, RoleRepository roleRepository, UserRepository userRepository, CommunityRepository communityRepository) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }

    @Override
    public User createUser(UserDto userDto) throws IOException {
        User user = new User();
        User userExists = userRepository.findUserByEmail(userDto.getEmail());

        byte[] fileContent = Files.readAllBytes(Paths.get("src/web/images/default-user.png"));
        Image image = new Image(fileContent, "default-user.png", "image/png");

        if(userExists == null) {
            user.setEmail(userDto.getEmail());
            user.setName(userDto.getName());
            user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
            user.setRoles(roleRepository.findUsersByRole("USER"));
            user.setDateAdded(LocalDateTime.now());
            user.setUserProfilePicture(image);
            userRepository.save(user);
        } else {
            throw new ApiRequestException("Email exists!");
        }
        return user;
    }

    @Override
    public User updateUser(UserDto userDto, UUID userId) {
        User user = userRepository.findUserById(userId);

        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        userRepository.save(user);

        return user;
    }

    @Override
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User findUserById(UUID id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    @Override
    public void deleteUser(UUID id) {
        userRepository.deleteById(id);
    }

    @Override
    public boolean isUserInCommunity(String userEmail, String communityName) {
        return userRepository.isUserInCommunity(userEmail, communityName);
    }
}
