package blog.sample.app.dto;

import java.util.UUID;

public class JoinedCommunitiesDto {
    private UUID userId;
    private Long communityId;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }
}
