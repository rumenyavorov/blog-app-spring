package blog.sample.app.dto.response;

import java.io.Serializable;

public class JwtAuthenticationReponse {
    private String accessToken;

    public JwtAuthenticationReponse(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
