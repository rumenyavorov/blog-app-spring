package blog.sample.app.repository;

import blog.sample.app.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    @Query("select r from Role r where r.roleName = :roleName")
    Set<Role> findUsersByRole(String roleName);
}
