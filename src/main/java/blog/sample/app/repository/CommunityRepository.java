package blog.sample.app.repository;

import blog.sample.app.model.Community;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface CommunityRepository extends JpaRepository<Community, Long> {
    @Query("select c from Community c")
    List<Community> findAllCommunities();

    @Query("select c from Community c where c.name = :name")
    Community findCommunityByName(String name);

    @Query("select c from Community c where c.id = :communityId")
    Community findCommunityById(Long communityId);

    @Query("select c, u from Community c, User u join u.communities jc where u.id = :userId and c.id = jc.id")
    List<Community> getJoinedCommunities(UUID userId);

//    @Query("select jc from User u join u.communities jc where jc.id = :t and ")
//    Community leaveCommunity();
}
