package blog.sample.app.repository;

import blog.sample.app.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Query("select com from Comment com where com.commentId = :commentId")
    Comment findCommentById(Long commentId);

    @Query("select com from Comment com where com.post.id = :postId order by com.date desc")
    List<Comment> findAllCommentsForPost(Long postId);

    @Query("select com from Comment com where com.user.id = :userId order by com.date desc")
    List<Comment> findAllCommentsByUser(UUID userId);
}
