package blog.sample.app.repository;

import blog.sample.app.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface PostRepository extends JpaRepository<Post, Long> {
    @Query("select p from Post p")
    List<Post> getAllPosts();

    @Query("select p from Post p where p.author.id = :author")
    List<Post> getAllPostsByAuthor(UUID author);

    @Query("select c.posts, jc " +
            "from Community c " +
            "join c.user ju " +
            "join ju.communities jc " +
            "where c.id = jc.id " +
            "and ju.id = :userId")
    List<Post> getAllPostsByUserJoinedCommunities(UUID userId);

    @Query("select p from Post p where p.id = :postId")
    Post findPostById(Long postId);
}
