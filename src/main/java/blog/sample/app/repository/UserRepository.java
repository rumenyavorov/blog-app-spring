package blog.sample.app.repository;

import blog.sample.app.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    @Query("select u from User u")
    List<User> getAllUsers();

    @Query("select u from User u where u.id = :id")
    User findUserById(UUID id);

    @Query("select u from User u where u.email = :email")
    User findUserByEmail(String email);

    @Query("select case when count(u) > 0 then true else false end from User u, Community c join u.communities jc" +
            " where u.email = :userEmail and jc.name = :communityName")
    boolean isUserInCommunity(String userEmail, String communityName);
}
