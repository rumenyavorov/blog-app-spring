package blog.sample.app.rest;

import blog.sample.app.components.JwtManager;
import blog.sample.app.dto.response.ApiResponse;
import blog.sample.app.dto.UserDto;
import blog.sample.app.dto.UserLoginDto;
import blog.sample.app.dto.response.JwtAuthenticationReponse;
import blog.sample.app.exception.ApiRequestException;
import blog.sample.app.model.User;
import blog.sample.app.repository.UserRepository;
import blog.sample.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/v1")
@CrossOrigin(origins = "http://localhost:4200")
public class AuthRest {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtManager jwtManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping(value = "/public/login")
    public ResponseEntity<?> login(@RequestBody UserLoginDto userLoginDto){
        User user = userRepository.findUserByEmail(userLoginDto.getEmail());
        if(userRepository.findUserByEmail(userLoginDto.getEmail()) == null){
            throw new ApiRequestException("Email doesn't exist!");
        }

        boolean passwordCheck = passwordEncoder.matches(userLoginDto.getPassword(), user.getPassword());

        if(!passwordCheck) {
            throw new ApiRequestException("Invalid email or password!");
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        userLoginDto.getEmail(),
                        userLoginDto.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtManager.generateToken(authentication);

        return ResponseEntity.ok(new ApiResponse(true, "Token generated successfully!", new JwtAuthenticationReponse(jwt)));
    }
}
