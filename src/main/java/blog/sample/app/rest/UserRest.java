package blog.sample.app.rest;

import blog.sample.app.dto.UserDto;
import blog.sample.app.dto.response.ApiResponse;
import blog.sample.app.model.User;
import blog.sample.app.service.ImageService;
import blog.sample.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/api/v1")
public class UserRest {

    private final UserService userService;

    private final ImageService imageService;

    @Autowired
    public UserRest(UserService userService, ImageService imageService) {
        this.userService = userService;
        this.imageService = imageService;
    }

    @PostMapping(value = "/public/register")
    public ResponseEntity<?> createNewUser(@RequestBody UserDto userDto) throws IOException {
        userService.createUser(userDto);
        return new ResponseEntity<>(new ApiResponse(true, "Register successfully!"), HttpStatus.OK);
    }

    @GetMapping(value = "/private/profile")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<User> profilePage(){
        User loggedInUser = (User) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User user = userService.findUserByEmail(loggedInUser.getEmail());

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PutMapping(value = "/private/user/{userId}")
    public ResponseEntity<?> editUser(@RequestBody UserDto userDto,
                                      @PathVariable("userId")UUID userId) {
        userService.updateUser(userDto, userId);
        return new ResponseEntity<>(new ApiResponse(true, "User update successfully."),
                HttpStatus.OK);
    }

    @GetMapping(value = "/private/user/{userEmail}/community/{communityName}")
    public ResponseEntity<Boolean> isJoined(@PathVariable("userEmail") String userEmail,
                                      @PathVariable("communityName") String communityName) {
        Boolean isUserJoined = userService.isUserInCommunity(userEmail, communityName);
        return new ResponseEntity<>(isUserJoined, HttpStatus.OK);
    }

    @PostMapping(value = "/private/user/{userEmail}/upload")
    public ResponseEntity<?> uploadProfilePicture(@RequestParam("image") MultipartFile imageFile,
                                                  @PathVariable("userEmail") String userEmail) throws IOException {
        imageService.userProfilePictureUpload(imageFile, userEmail);
        return new ResponseEntity<>(new ApiResponse(true, "Uploaded successfully"), HttpStatus.OK);
    }

    @GetMapping(value = "/private/user/{userEmail}")
    public ResponseEntity<User> getUserByEmail(@PathVariable("userEmail") String userEmail) {
        User user = userService.findUserByEmail(userEmail);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }
}
