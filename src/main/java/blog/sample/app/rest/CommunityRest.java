package blog.sample.app.rest;

import blog.sample.app.dto.CommunityDto;
import blog.sample.app.dto.response.ApiResponse;
import blog.sample.app.model.Community;
import blog.sample.app.service.CommunityService;
import blog.sample.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.*;

@RestController
@CrossOrigin(value = "*")
@RequestMapping(value = "/api/v1")
public class CommunityRest {

    private final CommunityService communityService;

    @Autowired
    public CommunityRest(CommunityService communityService) {
        this.communityService = communityService;
    }

    @GetMapping(value = "/public/community")
    public ResponseEntity<List<Community>> getAllCommunities() {
        List<Community> communities = communityService.getAllCommunities();
        return new ResponseEntity<>(communities, HttpStatus.OK);
    }

    @PostMapping(value = "/private/community")
    public ResponseEntity<Community> createCommunity(@RequestBody CommunityDto communityDto) throws IOException {
        Community community = communityService.createCommunity(communityDto);
        return new ResponseEntity<>(community, HttpStatus.OK);
    }

    @DeleteMapping(value = "/private/admin/community/{communityId}")
    public ResponseEntity<?> deleteCommunity(@PathVariable("communityId") Long communityId) {
        communityService.deleteCommunity(communityId);
        return new ResponseEntity<>(new ApiResponse(true, "Community deleted."), HttpStatus.OK);
    }

    @GetMapping(value = "/private/community/{communityId}/user/{userId}")
    public ResponseEntity<?> joinCommunity(@PathVariable("userId") UUID userId,
                                           @PathVariable("communityId") Long communityId) {
        communityService.joinCommunity(communityId, userId);

        return new ResponseEntity<>(new ApiResponse(true, "Community joined"),
                HttpStatus.OK);
    }

    @GetMapping(value = "/private/community/{communityId}")
    public ResponseEntity<?> leaveCommunity(@PathVariable("communityId") Long communityId) {
        communityService.leaveCommunity(communityId);

        return new ResponseEntity<>(new ApiResponse(true, "Community left."), HttpStatus.OK);
    }

    @GetMapping(value = "/private/community/user/{userId}")
    public ResponseEntity<?> communitiesJoined(@PathVariable("userId") UUID userId) {
        List<Community> joinedCommunitiesByUser = communityService.getJoinedCommunities(userId);
        return new ResponseEntity<>(joinedCommunitiesByUser, HttpStatus.OK);
    }

    @GetMapping(value = "/public/community/{communityName}")
    public ResponseEntity<?> viewCommunity(@PathVariable("communityName") String communityName) {
        Community community = communityService.viewCommunity(communityName);

        return new ResponseEntity<>(community, HttpStatus.OK);
    }
}
