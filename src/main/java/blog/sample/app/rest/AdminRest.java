package blog.sample.app.rest;

import blog.sample.app.dto.response.ApiResponse;
import blog.sample.app.model.User;
import blog.sample.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/v1/")
@CrossOrigin(value = "*")
public class AdminRest {

    @Autowired
    private UserService userService;

    @GetMapping("/private/admin/users")
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userService.findAllUsers();

        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @DeleteMapping("/private/admin/users/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") UUID id) {
        User userToDelete = userService.findUserById(id);
        userService.deleteUser(userToDelete.getId());

        return new ResponseEntity<>(new ApiResponse(true, "User successfully deleted"),
                HttpStatus.OK);
    }
}
