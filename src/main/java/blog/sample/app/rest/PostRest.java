package blog.sample.app.rest;

import blog.sample.app.dto.PostDto;
import blog.sample.app.dto.response.ApiResponse;
import blog.sample.app.model.Post;
import blog.sample.app.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(value = "*")
@RequestMapping(value = "/api/v1")
public class PostRest {
    @Autowired
    private PostService postService;

    @GetMapping("/public/post")
    public ResponseEntity<?> getAllPosts() {
        List<Post> post = postService.getAllPosts();

        return new ResponseEntity<>(post, HttpStatus.OK);
    }

    @PostMapping("/private/post")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> createPost(@RequestBody PostDto postDto) throws IOException {
        Post post = postService.createPost(postDto);
        return new ResponseEntity<>(post, HttpStatus.OK);
    }

    @PutMapping("/private/post/{id}")
    public ResponseEntity<?> updatePost(@PathVariable("id") Long id, @RequestBody PostDto postDto){
        postService.updatePost(id, postDto);

        return new ResponseEntity<>(new ApiResponse(true, "Post updated."), HttpStatus.OK);
    }

    @DeleteMapping("/private/post/{id}")
    public ResponseEntity<?> deletePost(@PathVariable("id") Long id) {
        postService.deletePost(id);

        return new ResponseEntity<>(new ApiResponse(true, "Post deleted."), HttpStatus.OK);
    }

    @GetMapping("/public/postsByAuthor/{id}")
    public ResponseEntity<?> getAllPostsByAuthor(@PathVariable(value="id")UUID id) {
        List<Post> postsByAuthor = postService.getAllPostsByAuthor(id);

        return new ResponseEntity<>(postsByAuthor, HttpStatus.OK);
    }

    @GetMapping(value = "/private/post/user/{userId}/communities")
    public ResponseEntity<List<Post>> communitiesUserJoined(@PathVariable("userId") UUID userId) {
        List<Post> posts = postService.getAllPostsByJoinedCommunities(userId);
        return new ResponseEntity<>(posts, HttpStatus.OK);
    }

    @GetMapping(value = "/public/post/{postId}")
    public ResponseEntity<?> viewPost(@PathVariable("postId") Long postId) {
        Post post = postService.findPostById(postId);
        return new ResponseEntity<>(post, HttpStatus.OK);
    }
}
