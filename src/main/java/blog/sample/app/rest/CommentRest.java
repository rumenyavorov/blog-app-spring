package blog.sample.app.rest;

import blog.sample.app.dto.CommentDto;
import blog.sample.app.dto.response.ApiResponse;
import blog.sample.app.model.Comment;
import blog.sample.app.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/v1/")
@CrossOrigin(value = "*")
public class CommentRest {

    @Autowired
    private CommentService commentService;

    @PostMapping(value = "/private/post/{postId}/comments")
    public ResponseEntity<?> createComment(@PathVariable("postId") Long postId,
                                           @RequestBody CommentDto commentDto) {
        Comment comment = commentService.createComment(postId, commentDto);

        return new ResponseEntity<>(comment, HttpStatus.OK);
    }

    @GetMapping(value = "/private/post/{postId}/comments")
    public ResponseEntity<?> viewCommentForPost(@PathVariable("postId") Long postId) {
        List<Comment> comment = commentService.findAllCommentsForPost(postId);

        return new ResponseEntity<>(comment, HttpStatus.OK);
    }

    @DeleteMapping(value = "/private/post/{postId}/comments/{commentId}")
    public ResponseEntity<?> deleteComment(@PathVariable("postId") Long postId,
                                           @PathVariable("commentId") Long commentId) {
        commentService.deleteComment(commentId);

        return new ResponseEntity<>(new ApiResponse(true, "Comment deleted."), HttpStatus.OK);
    }

    @GetMapping(value = "/private/user/{userId}/comments")
    public ResponseEntity<?> findCommentsByUser(@PathVariable("userId") UUID userId) {
        List<Comment> comments = commentService.findAllCommentsByUser(userId);

        return new ResponseEntity<>(comments, HttpStatus.OK);
    }
}
