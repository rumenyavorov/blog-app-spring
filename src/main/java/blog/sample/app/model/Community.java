package blog.sample.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "communities")
public class Community {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "community_name", unique = true, length = 60, nullable = false)
    private String name;

    @Column(name = "community_description", nullable = false)
    private String description;

    private LocalDateTime dateAdded;

    @OneToMany(mappedBy = "community", cascade = CascadeType.PERSIST)
    private List<Post> posts;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "communities", cascade = CascadeType.PERSIST)
    @JsonIgnoreProperties(value = {"email", "name", "dateAdded", "roles", "communities" })
    private Set<User> user = new HashSet<User>();

    @ManyToOne
    @JoinColumn(name = "community_author", referencedColumnName = "user_id")
    @JsonIgnoreProperties(value = {"email", "roles", "communities", "communityList", "dateAdded"})
    private User communityAuthor;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "image_id", referencedColumnName = "image_id")
    @JsonIgnoreProperties(value = { "imageId", "imageType", "imageName", "communityImage", "userProfilePicture", "postImage" })
    private Image communityImage;

    public Community() {
    }

    public Community(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<User> getUser() {
        return user;
    }

    public void setUser(Set<User> user) {
        this.user = user;
    }

    public LocalDateTime getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(LocalDateTime dateAdded) {
        this.dateAdded = dateAdded;
    }

    public User getCommunityAuthor() {
        return communityAuthor;
    }

    public void setCommunityAuthor(User communityAuthor) {
        this.communityAuthor = communityAuthor;
    }

    public Image getCommunityImage() {
        return communityImage;
    }

    public void setCommunityImage(Image communityImage) {
        this.communityImage = communityImage;
    }

    @Override
    public String toString() {
        return "Community{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
