package blog.sample.app.model;

import org.springframework.security.core.Transient;

import javax.persistence.*;
import java.util.Arrays;

@Entity
public class Image {
    @Id
    @Column(name = "image_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long imageId;

    @Column(name = "image_name")
    private String imageName;

    @Column(name = "image_type")
    private String imageType;

    @Lob
    @Column(name = "image")
    private byte[] image;

    @OneToOne(mappedBy = "userProfilePicture")
    private User userProfilePicture;

    @OneToOne(mappedBy = "postImage")
    private Post postImage;

    @OneToOne(mappedBy = "communityImage")
    private Community communityImage;

    public Image() {

    }

    public Image(byte[] image, String imageName, String imageType) {
        this.image = image;
        this.imageName = imageName;
        this.imageType = imageType;
    }

    public Image(byte[] image) {
        this.image = image;
    }

    public Long getImageId() {
        return imageId;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public User getUserProfilePicture() {
        return userProfilePicture;
    }

    public void setUserProfilePicture(User userProfilePicture) {
        this.userProfilePicture = userProfilePicture;
    }

    public Post getPostImage() {
        return postImage;
    }

    public void setPostImage(Post postImage) {
        this.postImage = postImage;
    }

    public Community getCommunityImage() {
        return communityImage;
    }

    public void setCommunityImage(Community communityImage) {
        this.communityImage = communityImage;
    }

    @Override
    public String toString() {
        return "Image{" +
                "imageId=" + imageId +
                ", imageName='" + imageName + '\'' +
                ", imageType='" + imageType + '\'' +
                ", image=" + Arrays.toString(image) +
                '}';
    }
}
