package blog.sample.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import javax.validation.constraints.Max;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "user_id")
    private UUID id;

    @Column(name = "user_email", unique = true, length = 60, nullable = false)
    private String email;

    @Column(name = "user_full_name", length = 60, nullable = false)
    private String name;

    @Column(name = "user_password", length = 90, nullable = false)
    @JsonIgnore
    private String password;

    private LocalDateTime dateAdded;

    @ManyToMany
    @JoinTable(name = "users_roles",
        joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "role_id", referencedColumnName = "role_id")}
    )
    @JsonIgnoreProperties(value = {"users"}, ignoreUnknown = true)
    private Set<Role> roles = new HashSet<>();

    @OneToMany(mappedBy = "author", cascade = CascadeType.PERSIST)
    @JsonIgnore
    private List<Post> posts;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "joined_communities",
                joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "user_id") },
                    inverseJoinColumns = { @JoinColumn(name = "community_id", referencedColumnName = "id") }
    )
    @JsonIgnoreProperties(value = {"user", "name", "description", "posts", "dateAdded", "communityAuthor"}, ignoreUnknown = true)
    private Set<Community> communities = new HashSet<Community>();

    @OneToMany(mappedBy = "communityAuthor", cascade = CascadeType.PERSIST)
    @JsonIgnore
    private List<Community> communityList;

    @OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST)
    @JsonIgnore
    private List<Comment> comments;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "image_id", referencedColumnName = "image_id")
    @JsonIgnoreProperties(value = { "imageId", "imageType", "imageName", "userProfilePicture", "postImage", "communityImage" })
    private Image userProfilePicture;


    public User(String email, String s, List<GrantedAuthority> authorities) {
        this.email = email;
    }

    public User() {

    }

    public User(String name) {
        this.name = name;
    }

    public User(String email, String name, String password, Set<Role> roles) {
        this.email = email;
        this.name = name;
        this.password = password;
        this.roles = roles;
    }

    public void removeFromCommunity(Community community) {
        this.communities.remove(community);
        community.getUser().remove(this);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public LocalDateTime getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(LocalDateTime dateAdded) {
        this.dateAdded = dateAdded;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public Set<Community> getCommunities() {
        return communities;
    }

    public void setCommunities(Set<Community> communities) {
        this.communities = communities;
    }

    public List<Community> getCommunityList() {
        return communityList;
    }

    public void setCommunityList(List<Community> communityList) {
        this.communityList = communityList;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Image getUserProfilePicture() {
        return userProfilePicture;
    }

    public void setUserProfilePicture(Image userProfilePicture) {
        this.userProfilePicture = userProfilePicture;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", dateAdded=" + dateAdded +
                ", roles=" + Arrays.toString(roles.toArray()) +
                ", joined_communities=" + Arrays.toString(communities.toArray()) +
                '}';
    }
}
